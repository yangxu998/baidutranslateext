/**
 * Copyright (C) 2014 yangxu998@gmail.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
function translate(info){
	if(info.selectionText != null && info.selectionText.trim() != null){
		$.ajax({
			url : "http://openapi.baidu.com/public/2.0/bmt/translate?client_id=A6zoYQj2ogD6UCQnLTag4EUH&q=" + info.selectionText.trim() + "&from=auto&to=auto",
			success: function(data){
				if(data.trans_result != undefined){
					var notification = webkitNotifications.createNotification(
						'icons/icon48.png',
						'百度翻译',
						data.trans_result[0].dst
					);

					notification.show();
					setTimeout(function(){
						notification.close();
						notification.cancel();
					}, 10000);
				}
			}
		});
		return;
	}

	chrome.tabs.getSelected(null, function(tab) {
		chrome.tabs.sendRequest(tab.id, {}, function(response){});
	});
}

chrome.contextMenus.create({
	"title" : "使用百度翻译翻译当前页",
	"contexts" : ["page", "frame"],
	"onclick" : translate
});

chrome.contextMenus.create({
	"title" : "使用百度翻译翻译%s",
	"contexts" : ["selection"],
	"onclick" : translate
});
