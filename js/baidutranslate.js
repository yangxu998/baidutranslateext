/**
 * Copyright (C) 2014 yangxu998@gmail.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
chrome.extension.onRequest.addListener(function(request, sender, sendResponse){
	translate(document.body);
});

function translate(doc){
	var children = doc.childNodes;
	if(children != null && children.length > 0){
		for(var i = 0; i < children.length; i++){
			translate(children[i]);
		}
		return;
	}

	if(doc.parentNode.tagName != "SCRIPT" && doc.nodeType == 3 && doc.textContent.trim() != ""){
		$.ajax({
			url : "http://openapi.baidu.com/public/2.0/bmt/translate?client_id=A6zoYQj2ogD6UCQnLTag4EUH&q=" + urlencode(doc.textContent.trim()) + "&from=auto&to=auto",
			success: function(data){
				if(data.trans_result != undefined){
					var wrapText = "<span title='" + doc.textContent + "'></span>";
					doc.textContent = "";
					for(var i = 0;  i < data.trans_result.length; i++){
						doc.textContent += data.trans_result[i].dst;	
					}
					$(doc).wrap(wrapText);
				}
			}
		});
	}

}

function urlencode(str) {
    str = (str + '').toString();
    return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29')
        .replace(/\*/g, '%2A').replace(/%20/g, '+');
}